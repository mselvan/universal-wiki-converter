package com.atlassian.uwc.util;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

public class CustomProperties {
	private static final String SEPERATOR = "=";
    static Logger log = Logger.getLogger(CustomProperties.class);

    private static Map<String, String> map = null;

    public static Map<String, String> getProperties() throws IOException {
    	if(map == null) {
    		map = loadPropertiesFile();
    	}
    	return map;
    }

    /**
     * Read in a standard properties file to a TreeMap
     * @param fileLoc
     * @return a map which matches the properties file
     * @throws IOException
     */
    private static Map<String,String> loadPropertiesFile() throws IOException {
    	String fileLoc = "conf" + File.separator + "custom.properties";
        Map<String,String> map = new TreeMap<>();
        File inputFile = new File(fileLoc);
        if (!inputFile.exists()) {
            log.error("Property file not found: "+fileLoc);
            return null;
        }
        RandomAccessFile ram = new RandomAccessFile(inputFile,"r");
        String line = null;
        while ((line=ram.readLine())!=null) {
            if (line.startsWith("#")) continue;
            int seperatorLoc = line.indexOf(SEPERATOR);
            if (seperatorLoc<=0) continue;
            String key = line.substring(0,seperatorLoc);
            String value = line.substring(seperatorLoc+1);
            map.put(key,value);
        }
        ram.close();
        return map;
    }
}
